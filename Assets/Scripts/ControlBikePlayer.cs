﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBikePlayer : MonoBehaviour {

    public float speed;
    public GameObject colObject;
    public GameObject trailRen;
    public GameObject newColObject;
    public bool lost = false;
    public bool won = false;
    public bool destroyed = false;
    public GameObject explosion;

    private Vector3 pos;
    private Vector3 startMarker;
    private Vector3 endMarker;
    private Vector3 width;
    private Vector3 colSizeMultiplier;
    private BikePlayerFollow followCam;


    // Use this for initialization
    void Start () {

        followCam = Camera.main.GetComponent<BikePlayerFollow>();

        startMarker = transform.position + transform.up;
        endMarker = transform.position;
        newColObject = Instantiate(colObject, startMarker, Quaternion.identity);
        pos = transform.position;
    }
	
	// Update is called once per frame
	void Update () {


        startMarker = transform.position + transform.up;

        if (!destroyed)
        {
            transform.position += transform.forward * speed * Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (!destroyed)
            {
                transform.Rotate(0, 90f, 0f);
                endMarker = transform.position;
                newColObject = Instantiate(colObject, startMarker, Quaternion.identity);
                followCam.defaultDist = new Vector3(1.5f, 4f, -10f);
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (!destroyed)
            {
                transform.Rotate(0, -90f, 0f);
                endMarker = transform.position;
                newColObject = Instantiate(colObject, startMarker, Quaternion.identity);
                followCam.defaultDist = new Vector3(-1.5f, 4f, -10f);
            }
        }

        Vector3 dir = transform.forward;
        float dist = Vector3.Distance(startMarker,endMarker);


        //Forward direction X or Z
        if(dir.x == 0)
        {
            colSizeMultiplier = new Vector3(0, 0, 1f);
            width = new Vector3(1f, 0, 0);
        }
        else if (dir.z < 0.1 || dir.z > -0.9)
        {
            colSizeMultiplier = new Vector3(1f, 0, 0);
            width = new Vector3(0, 0, 1f);
        }

        newColObject.GetComponent<BoxCollider>().size = (colSizeMultiplier * dist) + new Vector3(0, 1f, 0) + width;
        newColObject.transform.position = (startMarker + endMarker) / 2;
        //newColObject.GetComponent<BoxCollider>().center = (startMarker + endMarker) / 2;

    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawSphere(startMarker,0.5f);
        Gizmos.DrawSphere(endMarker, 0.5f);
    }

}
