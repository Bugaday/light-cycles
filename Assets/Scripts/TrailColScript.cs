﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailColScript : MonoBehaviour {

    private UIGameOver uiGameOver;
    private ControlBikePlayer controlBikePlayer;
    private ControlBikeAI controlBikeAI;

    // Use this for initialization
    void Start () {
        uiGameOver = FindObjectOfType<UIGameOver>();
        controlBikePlayer = FindObjectOfType<ControlBikePlayer>();
        controlBikeAI = FindObjectOfType<ControlBikeAI>();
    }
	
	// Update is called once per frame
	void Update () {

	}

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject == controlBikePlayer.gameObject)
        {
            controlBikePlayer.transform.position += (controlBikePlayer.transform.forward * 2f);
            controlBikePlayer.destroyed = true;
            controlBikeAI.won = true;
            controlBikePlayer.GetComponent<MeshRenderer>().enabled = false;
            controlBikePlayer.explosion.SetActive(true);
            uiGameOver.DisplayYouLose();
        }
        else if(col.gameObject == controlBikeAI.gameObject)
        {
            print("AI collided with: " + this.name);
            print("AI transform.forward is: " + controlBikeAI.transform.forward);
            controlBikeAI.transform.position += (controlBikeAI.transform.forward * 2f);
            controlBikeAI.destroyed = true;
            controlBikeAI.lost = true;
            controlBikeAI.GetComponent<MeshRenderer>().enabled = false;
            controlBikeAI.explosion.SetActive(true);
            uiGameOver.DisplayYouWin();
        }
    }

}
