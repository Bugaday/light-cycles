﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameOver : MonoBehaviour {

    private Text text;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
	}

    public void DisplayYouWin()
    {
        text.text = "You Win!";
    }

    public void DisplayYouLose()
    {
        text.text = "You Lose!";
    }

}
