﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBikeAI : MonoBehaviour {

    public float speed = 10f;
    public GameObject colObject;
    public GameObject trailRen;
    public bool lost = false;
    public bool won = false;
    public bool destroyed = false;
    public GameObject explosion;

    public float moveStep;

    public Transform destMarker;

    private int colNameInt;
    private float turnTime;

    private Vector3 pos;
    private Vector3 startMarker;

    private MarkerObj northMarker;
    private MarkerObj eastMarker;
    private MarkerObj westMarker;

    private GameObject newColObject;
    private Vector3 width;
    private Vector3 colSizeMultiplier;
    private Vector3 endMarker;
    private enum States {noTurn, left, right };
    private States currentState;

    // Use this for initialization
    void Start () {
        colNameInt = 1;

        newColObject = Instantiate(colObject,startMarker,Quaternion.identity);
        newColObject.name = "AI_Collider_" + colNameInt.ToString();
        colNameInt++;

        pos = transform.position;

        startMarker = transform.position;
        endMarker = transform.position;

        northMarker = transform.GetChild(2).GetComponent<MarkerObj>();
        eastMarker = transform.GetChild(3).GetComponent<MarkerObj>();
        westMarker = transform.GetChild(4).GetComponent<MarkerObj>();


        currentState = States.noTurn;

        turnTime = Random.Range(1f, 2f);
    }

    // Update is called once per frame
    void Update() {

        moveStep = speed * Time.deltaTime;

        startMarker = transform.position + transform.up;

        //Move the bike constantly forward
        if (!won || !lost)
        {
            if (!destroyed)
            {
                transform.position = Vector3.MoveTowards(transform.position, destMarker.position, moveStep);
                transform.rotation = Quaternion.LookRotation(destMarker.position - transform.position);
            }
        }

        if (Vector3.Magnitude(transform.position - destMarker.position) <= moveStep)
        {
            if (!destroyed)
            {
                destMarker.position = transform.position + transform.right * Random.Range(5f, 30f);
                endMarker = transform.position;
                newColObject = Instantiate(colObject, startMarker, Quaternion.identity);
                newColObject.name = "AI_Collider_" + colNameInt.ToString();
                colNameInt++;
            }
        }

        //if (northMarker.inCollision)
        //{
        //    if (eastMarker.inCollision && !westMarker.inCollision)
        //    {
        //        currentState = States.left;
        //    }
        //    else if (westMarker.inCollision && !eastMarker.inCollision)
        //    {
        //        currentState = States.right;
        //    }
        //    else if (westMarker.inCollision && eastMarker.inCollision)
        //    {
        //        currentState = States.noTurn;
        //    }
        //    else
        //    {
        //        currentState = States.right;
        //    }
        //}
        //else
        //{
        //    currentState = States.noTurn;
        //}


        //Calling turn functions depending on state
        if (currentState == States.left)
        {
            TurnLeft();
        }

        if (currentState == States.right)
        {
            TurnRight();
        }

        Vector3 dir = transform.forward;
        float dist = Vector3.Distance(startMarker, endMarker);


        //Calculate trail collision box size
        //Forward direction X or Z
        if (dir.x == 0)
        {
            colSizeMultiplier = new Vector3(0, 0, 1f);
            width = new Vector3(1f, 0, 0);
        }
        else if (dir.z < 0.1 || dir.z > -0.9)
        {
            colSizeMultiplier = new Vector3(1f, 0, 0);
            width = new Vector3(0, 0, 1f);
        }

        Vector3 colSize = newColObject.GetComponent<BoxCollider>().size;
        colSize = transform.forward * dist + transform.right;
        //colSize.y = 1f;
        newColObject.transform.position = (startMarker + endMarker) / 2;



    }

    void TurnRight()
    {
        transform.Rotate(0, 90f, 0f);
        endMarker = transform.position;
        newColObject = Instantiate(colObject, startMarker, Quaternion.identity);
        currentState = States.noTurn;
    }

    void TurnLeft()
    {
        {
            transform.Rotate(0, -90f, 0f);
            endMarker = transform.position;
            newColObject = Instantiate(colObject, startMarker, Quaternion.identity);
            currentState = States.noTurn;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawSphere(startMarker, 0.5f);
        Gizmos.DrawSphere(endMarker, 0.5f);
    }

}
