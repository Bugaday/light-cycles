﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerObj : MonoBehaviour {

    private ControlBikeAI controlBikeAI;

    public bool inCollision = false;

	// Use this for initialization
	void Start () {
        controlBikeAI = transform.parent.GetComponent<ControlBikeAI>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter()
    {
        inCollision = true;
    }

    void OnTriggerExit()
    {
        inCollision = false;
    }
}
