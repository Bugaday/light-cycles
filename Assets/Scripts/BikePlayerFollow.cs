﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BikePlayerFollow : MonoBehaviour {

    public GameObject bikePlayer;
    public float posDamp = 10f;
    public float rotDamp = 10f;
    public Vector3 defaultDist = new Vector3(1.5f, 4f, -10f);

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
        Vector3 toPos = bikePlayer.transform.position + (bikePlayer.transform.rotation * defaultDist);
        Vector3 curPos = Vector3.Lerp(transform.position,toPos,posDamp * Time.deltaTime);

        Quaternion toRot = Quaternion.LookRotation(bikePlayer.transform.position - transform.position, bikePlayer.transform.up);
        Quaternion curRot = Quaternion.Slerp(transform.rotation, toRot, rotDamp * Time.deltaTime);

        transform.position = curPos;
        transform.rotation = curRot;
	}
}
